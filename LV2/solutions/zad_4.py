
"""
Zadatak 4

Simulirajte 100 bacanja igraće kocke (kocka s brojevima 1 do 6). Pomoću histograma prikažite rezultat ovih bacanja. 
"""

import numpy as np
import matplotlib.pyplot as plt

np.random.seed(56) 
randomNumbers = np.random.randint(low=1, high=7, size=100)

print('Dobiveni brojevi su:\n', randomNumbers)

plt.hist(randomNumbers, bins=20)
plt.xlabel("Znamenka")
plt.ylabel("Broj pojavljivanja")

