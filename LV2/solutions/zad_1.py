"""
Zadatak 1

Napišite Python skriptu koja će učitati tekstualnu datoteku, 
pronaći valjane email adrese te izdvojiti samo prvi dio
adrese (dio ispred znaka @). Koristite odgovarajući regularni izraz. 
Koristite datoteku mbox-short.txt. Ispišite rezultat. 
"""

import re

path = input('Uneite putanju do datoteke: ')
fhand = open(path)

allEmails = []
allEmails += re.findall('[a-zA-Z0-9_.+-]\S*@\S+[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+', fhand.read())

distinctEmails = list(dict.fromkeys(allEmails))
print('Valjane email adrese su:\n', distinctEmails)


names = []

for email in distinctEmails:
    names.append(email.split("@")[0])
    
print('\nImena su:\n',names)

