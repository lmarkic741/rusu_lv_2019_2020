
"""
Zadatak 2

Na temelju rješenja prethodnog zadatka izdvojite prvi dio adrese (dio ispred @ znaka) samo ako:
 1. sadrži najmanje jedno slovo a
 2. sadrži točno jedno slovo a
 3. ne sadrži slovo a
 4. sadrži jedan ili više numeričkih znakova (0 – 9)
 5. sadrži samo mala slova (a-z) 
"""

import re


path = input('Uneite putanju do datoteke: ')
fhand = open(path)

allEmails = []
names = []
namesWithOneAOrMore  = []
namesWithOneA  = []
namesWithoutA  = []
namesWithNumeric  = []
lowerCaseNames  = []


allEmails += re.findall('[a-zA-Z0-9_.+-]\S*@\S+[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+', fhand.read())

distinctEmails = list(dict.fromkeys(allEmails)) #uklonimo redundacije, odnosno ponavljanje istih email adresa
print('Sve elektronike pošte:\n\n',distinctEmails)


for email in distinctEmails:
    names.append(email.split("@")[0])
    

for name in  names:
    if(re.fullmatch(r'.*a+.*', name)):
        namesWithOneAOrMore.append(name)
    if(re.fullmatch(r'.*a.*', name)):
        namesWithOneA.append(name)
    if(not re.fullmatch(r'.*a+.*', name)):
        namesWithoutA.append(name)
    if(re.fullmatch(r'.*[0-9].*', name)):
        namesWithNumeric.append(name)
    if(re.fullmatch(r'^[a-z]+', name)):
        lowerCaseNames.append(name)


print('\n1. sadrži najmanje jedno slovo a\n')
for name in namesWithOneAOrMore:
    print(name)
    
print('\n2. sadrži točno jedno slovo a\n')
for name in namesWithOneA:
    print(name)
    
print('\n3. ne sadrži slovo a\n')
for name in namesWithoutA:
    print(name)
    
print('\n4. sadrži jedan ili više numeričkih znakova (0 – 9)\n')
for name in namesWithNumeric:
    print(name)
    
print('\n5. sadrži samo mala slova (a-z)\n')
for name in lowerCaseNames:
    print(name)
