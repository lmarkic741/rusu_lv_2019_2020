
"""
Zadatak 5

U direktoriju rusu_lv_2019_20/LV2/resources nalazi se datoteka mtcars.csv koja sadrži različita
mjerenja provedena na 32 automobila (modeli 1973-74). Prikažite ovisnost potrošnje automobila (mpg) o konjskim
snagama (hp). Na istom grafu prikažite i informaciju o težini pojedinog vozila. Ispišite minimalne, maksimalne i
srednje vrijednosti potrošnje automobila.
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

path = input('Uneite putanju do datoteke: ')
cars = pd.read_csv(path)


plt.scatter(cars.hp, cars.mpg, c = cars.wt)
plt.xlabel('Konjska snaga automobila (hp)')
plt.ylabel('Potrošnja automobila (mpg)')

print('Minimalna vrijednost potrosnje: ')
print(min(cars.mpg))
print('Maksimalna vrijednost potrosnje: ')
print(max(cars.mpg))
print('Prosjecna vrijednost potrosnje: ')
print(np.average(cars.mpg))

