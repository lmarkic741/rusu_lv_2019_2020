"""
Zadatak 6

Na temelju primjera 2.8. učitajte sliku 'tiger.png'. 
Manipulacijom odgovarajuće numpy matrice pokušajte posvijetliti
sliku (povećati brightness). 
"""

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

path = input('Uneite putanju do datoteke: ')

brightnessCoefficient = 1.5
image = mpimg.imread(path)
imageCopy = image.copy()
imageCopy *= brightnessCoefficient


plt.imshow(image)
plt.xlabel("Originalana slika")
plt.figure()

plt.imshow(imageCopy)
plt.xlabel(f"Slika nakon podešavanja svjetline s koeficjenotom {brightnessCoefficient}")