
"""
Zadatak 3

Napravite jedan vektor proizvoljne dužine koji sadrži slučajno generirane cjelobrojne vrijednosti 0 ili 1. Neka 1
označava mušku osobu, a 0 žensku osobu. Napravite drugi vektor koji sadrži visine osoba koje se dobiju uzorkovanjem
odgovarajuće distribucije. U slučaju muških osoba neka je to Gaussova distribucija sa srednjom vrijednošću 180 cm i
standardnom devijacijom 7 cm, dok je u slučaju ženskih osoba to Gaussova distribucija sa srednjom vrijednošću 167 cm
i standardnom devijacijom 7 cm. Prikažite podatke te ih obojite plavom (1) odnosno crvenom bojom (0). Napišite
funkciju koja računa srednju vrijednost visine za muškarce odnosno žene (probajte izbjeći for petlju pomoću funkcije
np.dot). Prikažite i dobivene srednje vrijednosti na grafu.
"""

import numpy as np
import matplotlib.pyplot as plt

um = 180 #Srednju vrijednost za muškarce
sm = 7 #Standardna devijacija za muškarce
uf = 167 #Srednju vrijednost za žene
sf = 7 #Standardna devijacija za žene

np.random.seed(56) 
genders=np.random.randint(2,size=20)

print('Generirane osobe:\n',genders)

maleHeights = []
femaleHeights = []

for gender in genders:
    if gender==1:
        maleHeights.append(np.random.normal(um,sm))
    else:
        femaleHeights.append(np.random.normal(uf,sf))


maleMeanHeight = np.average(maleHeights)
femaleMeanHeight = np.average(femaleHeights)
print(f"\nSrednja vrijednost visine za muškarce je: {maleMeanHeight} cm, a srednja vrijednost visine za žene je: {femaleMeanHeight} cm.\n")
plt.hist([maleHeights, femaleHeights] ,color=['blue','red'])
plt.axvline(maleMeanHeight, color='blue')
plt.axvline(femaleMeanHeight, color='red')
plt.xlabel("Visina [cm]")
plt.ylabel("Broj osoba")
