Kroz ovu vježbu upoznali smo se s bibliotekom za implementaciju regularnih izraza re, te smo kroz 
zadatke 1 i 2 koristimo tu biblioteku kako bi dobili odgovarajuće string vrijednosti iz zadanog niza (datoteke).

Druga važana biblioteka s kojom smo se upoznali je Numpy, to je biblioteka za numeričke proračune.
Glavan razlika u odnosu na python liste je ta što svi elementi moraju biti istog tipa (primjerice float, int i sl.).

Treća biliblioteka je Matplotlib koja sadrži sadrži različite funkcije za grafički prikaz. Omogućava nam iscrtavanje slika, 
histograma, prikaz podatak u koorinatnom sustavu i duge mogućnosti koristeći razne funkcije poput pyplot, imshow i slično.

Navedno smo iskoristili u zadacima 3, 4, 5 i 6. U zadatku 3 koristeći nasumične vrijednosti normalne razdiobe visina 
muškaraca i žena prikazan je histogram frekevencije visina kao i prosječna vrijenost.
U zadatku 4 nasumično je generiran niz znamanki igraće kocke korsiteći funkciju np.random.randint(low=1, high=7, size=100),
pri čemu low predstavalja početnu, a high krajnju vrjednost dok size predstavalja veličinu niza koju ćemo generirati.
U zadatku 5. učitali smo podatke iz csv datoteke i prikazali ovisnost potršnje automobila o konjskoj snazi, te smo prikazali i težine autombobila.
Za ostvarnje navednog je korištena bilioteka pandas koja na jednostavan način omogućuje pristup podacima iz učitane csv datoteke. Također
je izračunata minimalna, maksimalna i prosječna vrijdnost potrošnje automobila.
U zadatku 6. vrši se manuipulacija svjetlinom slike, navedno je ostvarno množenjem slike s danim koeficjentom. Ptikazom originalne i modificirane slike
moguće je uočiti razliku.

