import numpy as np
import matplotlib.pyplot as plt
import sklearn

from sklearn.linear_model import LogisticRegression


def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data


np.random.seed(240)
data_train = generate_data(200)
np.random.seed(12)
data_test = generate_data(100)


plt.figure()
plt.scatter(data_train[:,0], data_train[:,1], c=data_train[:,2], cmap='plasma')


model = sklearn.linear_model.LogisticRegression()
model.fit(data_train[:,0:2], data_train[:,2])


param = list(model.coef_[0])
param.append(model.intercept_[0])

plt.xlabel("x1")
plt.ylabel("x2")

x2 = -param[2]/param[1] - (param[0]/param[1])*data_train[:,0]
plt.plot(data_train[:, 0], x2)
plt.show()



plt.figure()
y_pred = model.predict(data_test[:,0:2])
colors_test = ["green" if yp == yt else "black" for yp, yt in zip(y_pred, data_test[:,2])]
plt.scatter(data_test[:,0], data_test[:,1], c = colors_test)
plt.plot(data_train[:, 0], x2)
plt.xlabel("x1")
plt.ylabel("x2")
plt.show()
