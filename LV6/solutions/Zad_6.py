import numpy as np
import matplotlib.pyplot as plt
import sklearn

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score

def generate_data(n):
    
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data  = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)    
    data = data[indices,:]
    
    return data

def plot_confusion_matrix(c_matrix):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()


np.random.seed(240)
data_train = generate_data(200)
np.random.seed(12)
data_test = generate_data(100)


plt.figure()
plt.scatter(data_train[:,0], data_train[:,1], c=data_train[:,2], cmap='plasma')


model = sklearn.linear_model.LogisticRegression()
model.fit(data_train[:,0:2], data_train[:,2])


param = list(model.coef_[0])
param.append(model.intercept_[0])

plt.xlabel("x1")
plt.ylabel("x2")

x2 = -param[2]/param[1] - (param[0]/param[1])*data_train[:,0]
plt.plot(data_train[:, 0], x2)
plt.show()

y_pred = model.predict(data_test[:,0:2])


confusionMatrix = sklearn.metrics.confusion_matrix(data_test[:, 2], y_pred)
plot_confusion_matrix(confusionMatrix)

acc = accuracy_score(data_test[:,2], y_pred)
print("Accuracy: ", acc)
prec = precision_score(data_test[:,2], y_pred)
print("Precision: ", prec)
rec = recall_score(data_test[:,2], y_pred)
print("Recall", rec)
miss = 1 - acc
print("Missclasification rate: ", miss)
spec = confusionMatrix[1, 1] / (confusionMatrix[1, 1] + confusionMatrix[0, 1])
print("Specificity: ", spec)