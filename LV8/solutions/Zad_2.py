import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential
import sklearn.metrics as sm
from tensorflow.keras.preprocessing import image_dataset_from_directory
import glob
import shutil
import os
import pandas as pd
import cv2

! pip install -q kaggle
!cp kaggle.json ~/.kaggle/kaggle.json
!mkdir ~/.kaggle
!kaggle datasets download -d
!kaggle datasets download -d meowmeowmeowmeowmeow/gtsrb-german-traffic-sign
!unzip gtsrb-german-traffic-sign -d treffic-sign/


test_dir_path = '/content/treffic-sign/'
test_dir_path_new = '/content/treffic-sign/Test_Dir/'

test_dir = pd.read_csv('/content/treffic-sign/Test.csv')
test_dir_lenght = test_dir.shape[0]

for i in range (0,test_dir_lenght):
   image_path = str(test_dir['Path'][i])
   class_id = str(test_dir['ClassId'][i])
   path = test_dir_path_new + class_id
   
   doesExist = os.path.exists(path)
   if not doesExist:
       os.makedirs(path)
   shutil.copy(test_dir_path + image_path, test_dir_path_new + class_id
               + '/' + image_path[4:])
  