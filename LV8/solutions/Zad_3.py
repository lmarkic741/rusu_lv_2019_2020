import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential
import sklearn.metrics as sm
from tensorflow.keras.preprocessing import image_dataset_from_directory
import glob
import shutil
import os
import pandas as pd
import cv2

! pip install -q kaggle
!cp kaggle.json ~/.kaggle/kaggle.json
!mkdir ~/.kaggle
!kaggle datasets download -d
!kaggle datasets download -d meowmeowmeowmeowmeow/gtsrb-german-traffic-sign
!unzip gtsrb-german-traffic-sign -d treffic-sign/

test_dir_path = '/treffic-sign/'
test_dir_path_new = '/treffic-sign/Test_Dir/'

test_dir = pd.read_csv('/treffic-sign/Test.csv')
test_dir_lenght = test_dir.shape[0]

#2
for i in range (0,test_dir_lenght):
   image_path = str(test_dir['Path'][i])
   class_id = str(test_dir['ClassId'][i])
   path = test_dir_path_new + class_id
   
   doesExist = os.path.exists(path)
   if not doesExist:
       os.makedirs(path)
   shutil.copy(test_dir_path + image_path, test_dir_path_new + class_id
               + '/' + image_path[4:])



#3
input_shape = (48, 48, 3)
num_classes = 43

 # ucitavanje train podataka iz odredenog direktorija
train_data = image_dataset_from_directory(
 directory='treffic-sign/Train/',
 labels='inferred',
 label_mode='categorical',
 batch_size=32,
 image_size=(48, 48))
# ucitavanje test podataka iz odredenog direktorija
test_data = image_dataset_from_directory(
 directory='treffic-sign/Test_Dir/',
 labels='inferred',
 label_mode='categorical',
 batch_size=32,
 image_size=(48, 48))

sequential_model = Sequential([
      keras.Input(shape=input_shape),
      layers.Conv2D(32, kernel_size=(3, 3), activation="relu", padding="same"),
      layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
      layers.MaxPooling2D(pool_size=(2, 2)),
      layers.Dropout(0.2),
      layers.Conv2D(64, kernel_size=(3, 3), activation="relu", padding="same"),
      layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
      layers.MaxPooling2D(pool_size=(2, 2)),
      layers.Dropout(0.2),
      layers.Conv2D(128, kernel_size=(3, 3), activation="relu", padding="same"),
      layers.Conv2D(128, kernel_size=(3, 3), activation="relu"),
      layers.MaxPooling2D(pool_size=(2, 2)),
      layers.Dropout(0.2),
      layers.Flatten(),
      layers.Dense(512, activation="relu"),
      layers.Dropout(0.5),
      layers.Dense(num_classes, activation="softmax"),
])

sequential_model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])
sequential_model.fit(train_data, epochs=5, batch_size=32)
sequential_model.summary()

accuracy = (sequential_model.evaluate(test_data, verbose=0))[1]
print("Točnost modela:", accuracy)