"""
Zadatak 2

Napišite programski kod koji će iscrtati sljedeće slike za mtcars skup podataka:
1. Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
2. Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara.
3. Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li automobili s ručnim mjenjačem veću
potrošnju od automobila s automatskim mjenjačem?
4. Prikažite na istoj slici odnos ubrzanja i snage automobila za automobile s ručnim odnosno automatskim
mjenjačem. 
"""


from matplotlib.legend import legend_handler
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

path = input('Unesite putanju do datoteke: ')
mtcars = pd.read_csv(path)

#1
fourCyl = mtcars[mtcars.cyl == 4]
sixCyl = mtcars[mtcars.cyl == 6]
eightCyl = mtcars[mtcars.cyl == 8]

plt.bar(fourCyl.car, fourCyl.mpg)
plt.bar(sixCyl.car, sixCyl.mpg)
plt.bar(eightCyl.car, eightCyl.mpg)

plt.legend(['4 cyl','6 cyl','8 cyl'])
plt.xticks(rotation=90)
plt.show()


#2
plt.figure()
plt.boxplot([fourCyl.wt, sixCyl.wt, eightCyl.wt], positions = [4,6,8])
plt.xlabel('cyl')
plt.ylabel('wt')
plt.show()

#3
plt.figure()
manual = mtcars[mtcars.am == 0].mpg
automatic = mtcars[mtcars.am == 1].mpg
plt.boxplot([manual, automatic], positions = [0,1], sym='k+')
plt.ylabel('mpg')
plt.show()
