"""
Zadatak 1 

Za mtcars skup podataka (nalazi se rusu_lv_2019_20/LV3/resources) napišite programski kod koji će 
odgovoriti na sljedeća pitanja: 
1. Kojih 5 automobila ima najveću potrošnju? (koristite funkciju sort) 
2. Koja tri automobila s 8 cilindara imaju najmanju potrošnju? 
3. Kolika je srednja potrošnja automobila sa 6 cilindara? 
4. Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs? 
5. Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka? 
6. Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga? 
7. Kolika je masa svakog automobila u kilogramima? 
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

path = input('Unesite putanju do datoteke: ')
mtcars = pd.read_csv(path)

#Ispis ucitanog dokumenta
print(mtcars) 

#1.
new_mtcars = mtcars.sort_values(by=['mpg'], ascending = False)
print('\n 5 automobila ima najveću potrošnju:\n', new_mtcars[['car', 'mpg']].head(5))

#2.
print('\n Tri automobila s 8 cilindara imaju najmanju potrošnju:\n', new_mtcars[new_mtcars.cyl == 8][['car', 'mpg', 'cyl']].tail(3))

#3.
sixCilnderCars = mtcars[mtcars.cyl == 6] 
print('\nSrednja potrosnja automobila sa 6 cilindara:', sixCilnderCars.mpg.mean())

#4
fourCilnderCars = mtcars.query('(cyl == 4) & (wt == [2.0, 2.2])')
print('\nSrednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs: ', fourCilnderCars.mpg.mean())

#5
print('\nBroj autombila s automatskim mjenjačem: ', mtcars[mtcars.am == 0].count().car)
print('\nBroj autombila s manualnim mjenjačem: ', mtcars[mtcars.am == 1].count().car)

#5
print('\nBroj autombila s automatskim mjenjačem i snagom preko 100 hp: ', mtcars[(mtcars.am == 0) & (mtcars.hp > 100)].count().car)

#6

mtcars['wtInKg'] = mtcars.wt *  0.45359237 * 1000
print('\nDatoteka s stupcem wtInKg koji predstavlja masu automobila iskazanu u kg\n',mtcars[['car', 'wt', 'wtInKg']])