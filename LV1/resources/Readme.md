Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci.
Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.

U skripti counting_words.py u 1. liniji posttavljen je inuput, promjenjeno je ime u 5. liniji u unutar open funkcije iz fnamex u fname.
U 7. i 20. liniji prepravljena je funkcija print (postavljene su zagrade). I dodano je else: grananje kod hvatanja iznimki.