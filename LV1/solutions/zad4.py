# -*- coding: utf-8 -*-
"""
"""

import statistics

numbers = []

while True:
        inpt = input('Unesite broj, a ukoliko želite završtit s unosom unesite Done: ')
        if inpt == 'Done' or inpt =='done':
            break
        else:
            try:
                number = int(inpt)
            except ValueError:
                print('Nije unešen broj!')
            else:
                numbers.append(number)

length = len(numbers)
mean = statistics.mean(numbers)
minvalue = min(numbers)
maxvalue = max(numbers)

print(f'Broj unesenih elemenata je: {length}\nProsječna vrijednost je: {mean}\nMinimalna vrijednost je: {minvalue}\nMaksimalna vrijednost je: {maxvalue}')
                
                
    