# -*- coding: utf-8 -*-
"""
"""

while True:
    try:
        grade = float(input('Unesite ocjenu u rasponu od 0.0 do 1.0: '))
        if grade >= 0.0 and grade <= 1.0:
            if grade >= 0.9:
                print('A')
            elif grade >= 0.8:
                print('B')
            elif grade >= 0.7:
                print('C')
            elif grade >= 0.6:
                print('D')
            else:
                print('F')
            break
        else:
            print('Broj nije u odgovarajućem rasponu!')
    except ValueError:
        print('Došlo je do pogreške')





