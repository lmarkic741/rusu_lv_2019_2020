# -*- coding: utf-8 -*-
"""
"""

import statistics

path = input('Unesite ime datoteke: ')
fhand = open(path)
numbers = []

for line in fhand:
 line = line.rstrip()
 if line.startswith('X-DSPAM-Confidence:'):
     splitted = line.split()
     number = float(splitted[1])
     numbers.append(number)
     print(number)
     
mean = statistics.mean(numbers)
print(f'\nSrednja vrijednost je: {mean}')