from tensorflow.keras.utils import img_to_array
from keras.models import load_model
from matplotlib import pyplot as plt
from skimage.transform import resize
from skimage import color
import matplotlib.image as mpimg
import numpy as np

filename = 'test.png'

img = mpimg.imread(filename)
img = color.rgb2gray((img))
img = resize(img, (28, 28))


plt.figure()
plt.imshow(img, cmap=plt.get_cmap('gray'))
plt.show()


img = img.reshape(1, 28, 28, 1)
img = img.astype('float32')


# TODO: ucitaj model
model = load_model("data")
model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])

# TODO: napravi predikciju 

predict_x=model.predict(img) 
classes_x=np.argmax(predict_x,axis=1)

# TODO: ispis rezultat
print("------------------------")
print("Predviđena znamenka je: ", classes_x[0])


#second part
filename = 'paintOfOne.png'
imgTwo = mpimg.imread(filename)
imgTwo = color.rgb2gray(imgTwo)
imgTwo = resize(imgTwo, (28, 28))

plt.figure()
plt.imshow(imgTwo, cmap=plt.get_cmap('gray'))
plt.show()

imgTwo = imgTwo.reshape(1, 28, 28, 1)
imgTwo = imgTwo.astype('float32')

predict_x=model.predict(imgTwo) 
classes_x=np.argmax(predict_x,axis=1)
# TODO: ispis rezultat
print("------------------------")
print("Predviđena znamenka je: ", classes_x[0])
