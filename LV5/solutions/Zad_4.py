import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from skimage import io

img = mpimg.imread('/content/example_grayscale.png')

try:
    face = sp.face(gray=True)
except AttributeError:
    from scipy import misc
    face = misc.face(gray=True)

face=img
X = face.reshape((-1, 1)) # We need an (n_sample, n_feature) array
k_means = cluster.KMeans(n_clusters=10,n_init=1)
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
face_compressed = np.choose(labels, values)
face_compressed.shape = face.shape

io.imsave('compressed_image_face.png', face_compressed)
plt.figure(1)
plt.title('Izvorna slika')
plt.imshow(face,  cmap='gray')


plt.figure(2)
plt.title('Komprimirana slika')
plt.imshow(face_compressed,  cmap='gray')

#Koprimirna slika je 2.57 puta manja od izvorene koriteći komresiju s 10 klastera