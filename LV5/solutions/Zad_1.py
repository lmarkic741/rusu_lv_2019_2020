from sklearn import datasets
import numpy as np
import sklearn.cluster as cluster
import matplotlib.pyplot as plt

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

X = generate_data(500, 1)

kMeans = cluster.KMeans(n_clusters = 3)

predict = kMeans.fit_predict(X)

plt.scatter(X[predict == 0, 0], X[predict == 0, 1], c = 'red', label = 'cluster 1')
plt.scatter(X[predict == 1, 0], X[predict == 1, 1], c = 'green', label = 'cluster 2')
plt.scatter(X[predict == 2, 0], X[predict == 2, 1], c = 'black', label = 'cluster 3')
plt.legend()

plt.scatter(kMeans.cluster_centers_[:,0], kMeans.cluster_centers_[:,1], color = 'blue') 
plt.show()

